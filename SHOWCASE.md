# Showcase your site!

This page is to showcase all the sites that are currently using the theme **biblioteca**. Don't hesitate to send a PR to add your site to this page!

- [koan](https://koan.cl "Blog in spanish about accessibility and software ethics"): a site in spanish, by the author of this theme, were we talk about accessibility issues and a bit about software ethics.