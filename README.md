# Biblioteca Hugo Theme

This is the repository for the **free theme**  (as in gratis) for the [Hugo static site generator](https://gohugo.io/ "Hugo official website"). To ensure that this project continues being *gratis*, I decided to licence it under the [Unlicense](https://unlicense.org/ "Unlicense official website"), go and take a look, hope you feel like you want to use that kind of licensing for your own projects.

It would be nice if you could add a link to this repository on your site if you use this theme, that way we can reach an even bigger audience, but that's completely up to you. If you do add a link, don't hesitate to send a PR adding your site to the [SHOWCASE.md file](SHOWCASE.md "Page with a list of sites using this theme").

This theme was started by [@joanca](https://gitlab.com/joanca/ "joanca's gitlab profile").

## Philosophy

This is an **opinionated theme** and a living software project that is and will be free software. It must follow the following principles:

1. **a11y compliant**: this theme must be a reference whenever we speak about web accessibility. I'm working on a way to do some automatic accesibility testing when submitting a PR.
2. **blazing fast**: by default this theme should be very fast, loading just the necessary stuff. Every script that you need to add to this theme **must** be only available in the DOM of the site, if explicitly stated on the `config.yaml` file.
3. You want to add another principle? Let's discuss!

Ok, but what is an opinionated theme? we enforce our principles, so our code must  be able to detect that. If we want to be accessible, we enforce that by design, so our theme will continue to be accessible. That means, for example, that we replaced the default `youtube` shortcode of Hugo, with a [custom one that embeds a video in a different way](/layouts/shortcodes/youtube.html "custom shortcode for embedding youtube videos"). This shortcode will **fail at compile time** if you don't provide a `title` for your video and you must set the `id` as a key/value pair.

So in other words, an opinionated theme is a theme that will force you to follow some design decisions to mantain a standard on how to use it when writing content using it. It also applies to adding new functionalities.
